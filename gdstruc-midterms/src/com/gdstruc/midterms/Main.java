package com.gdstruc.midterms;

public class Main {

    public static void main(String[] args) {

        CardStack deck = new CardStack(); // Initialize Deck Stack
        CardStack hand = new CardStack(); // Initialize Hand Stack
        CardStack discard = new CardStack(); // Initialize Dump Stack

        Card[] cardNames = new Card[4]; // Make predefined array of cardNames
        cardNames[0]= new Card("Hearts");
        cardNames[1]= new Card("Spades");
        cardNames[2]= new Card("Clubs");
        cardNames[3]= new Card("Diamonds");

        for(int i = 0; i < 30; i++){ // Generate Card Stack
            deck.push(cardNames[(int) (Math.random() * (3 + 1)+0)]);
        }


        while(!deck.isEmpty()){ //Main Iteration

            int roll = (int)(Math.random() * (3 - 1 + 1)+ 1);

            if (roll == 1){ // Draw X Cards from Deck

                System.out.println("Rolled 1st Command: Draw from Deck");

                int drawRoll = (int) (Math.random() * (5 - 1 + 1) + 1);

                System.out.println("Drawing " + drawRoll + " cards from deck");

                if (deck.sizeOf() == 0){
                    System.out.println("Deck is empty");

                }

                else if(drawRoll >= deck.sizeOf()){
                    for (int i = 0; i <= deck.sizeOf(); i++) { //Transfer using size of deck
                        hand.push(deck.peek());
                        deck.pop();
                    }

                }

                else {

                    for (int i = 0; i < drawRoll; i++) { //Transfer
                        hand.push(deck.peek());
                        deck.pop();
                    }
                }

            }

            else if (roll == 2){ // Discard X Cards from Hand

                System.out.println("Rolled 2nd Command: Discard from Hand");

                int drawRoll = (int) (Math.random() * (5 - 1 + 1) + 1);

                System.out.println("Discarding " + drawRoll + " cards from hand");

                 if(hand.sizeOf() == 0){
                    System.out.println("Hand is empty");
                }


                else if(drawRoll >= hand.sizeOf()){
                    for (int i = 0; i <= hand.sizeOf(); i++) { //Transfer using size of hand
                        discard.push(hand.peek());
                        hand.pop();
                    }
                }


                else {

                    for (int i = 0; i < drawRoll; i++) { //Transfer
                        discard.push(hand.peek());
                        hand.pop();
                    }

                }

            }

            else {

                System.out.println("Rolled 3rd Command: Draw from dump");

                int drawRoll = (int) (Math.random() * (5 - 1 + 1) + 1);

                System.out.println("Drawing " + drawRoll + " cards from discarded pile");

                if(discard.sizeOf() == 0){
                    System.out.println("Discard Pile is empty");
                }

                else if(drawRoll > discard.sizeOf()){
                    for (int i = 0; i <= discard.sizeOf(); i++) { //Transfer
                        hand.push(discard.peek());
                        discard.pop();
                    }
                }

                else {

                    for (int i = 0; i < drawRoll; i++) { //Transfer
                        hand.push(discard.peek());
                        discard.pop();
                    }

                }

            }

            System.out.println(" ");
            System.out.println("Player hand:");
            hand.printStack();
            printDecks(hand, discard, deck);

        }

    }

    public static void printDecks(CardStack hand, CardStack dump, CardStack deck){

        System.out.println(" ");
        System.out.println("Remaining Cards in Deck: " + deck.sizeOf());
        System.out.println("Remaining Cards in Hand: " + hand.sizeOf());
        System.out.println("Remaining Cards in Discarded Pile: " + dump.sizeOf());
        System.out.println(" ");

    }
}
