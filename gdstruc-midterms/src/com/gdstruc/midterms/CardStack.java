package com.gdstruc.midterms;

import java.util.EmptyStackException;
import java.util.LinkedList;
import java.util.ListIterator;

public class CardStack {

    private LinkedList<Card> stack;

    public CardStack(){

        stack = new LinkedList<Card>();

    }

    public void push(Card card){

        stack.push(card);

    }

    public boolean isEmpty(){
        return stack.isEmpty();
    }

    public  void  pop(){

        if(stack.isEmpty()){
            throw new EmptyStackException();
        }

        stack.pop();
    }

    public Card peek(){
        return stack.peek();
    }

    public int sizeOf(){
        return stack.size();
    }

    public void printStack(){
        ListIterator<Card> iterator = stack.listIterator();
        while (iterator.hasNext()){
            System.out.println(iterator.next());
        }


    }

}
