package com.gdstruc.quiz1;

public class Main {

    public static void main(String[] args) {

        int[] numbers = new int[10];

        numbers[0] = 35;
        numbers[1] = 69;
        numbers[2] = 1;
        numbers[3] = 10;
        numbers[4] = -50;
        numbers[5] = 320;
        numbers[6] = 63;
        numbers[7] = 58;
        numbers[8] = 26;
        numbers[9] = 13;


        System.out.println("\n\nBefore selection sort:");

        printArrayElements(numbers);

        System.out.println("\n\nAfter inverse selection sort:");

        modifiedSelectionSort(numbers);
        printArrayElements(numbers);


    }

    private static void printArrayElements(int[] arr) {

        for (int j : arr) {
            System.out.print(j + " ");
        }

    }

    private static void bubbleSort(int[] arr) {

        for (int lastSortedIndex = arr.length - 1; lastSortedIndex > 0; lastSortedIndex--) {

            for (int i = 0; i < lastSortedIndex; i++) {

                if (arr[i] <= arr[i + 1]) {

                    int temp = arr[i];
                    arr[i] = arr[i + 1];
                    arr[i + 1] = temp;

                }
            }
        }
    } //Descending Order

    private static void selectionSort(int[] arr) {

        for (int lastSortedIndex = arr.length - 1; lastSortedIndex > 0; lastSortedIndex--) {

            int largestIndex = 0;

            for (int i = 1; i <= lastSortedIndex; i++) {

                if (arr[i] < arr[largestIndex]) {
                    largestIndex = i;
                }

            }

            int temp = arr[lastSortedIndex];
            arr[lastSortedIndex] = arr[largestIndex];
            arr[largestIndex] = temp;
        }

    } //Descending Order

    private static void modifiedSelectionSort(int[] arr) {

        for (int i = arr.length - 1; i > 0; i--){
            int min_idx = 0;

            for(int j = i; j > 0; j--){

                if (arr[j] < arr[min_idx]){
                    min_idx = j;
                }
            }

            int temp = arr[i];
            arr[i] = arr[min_idx];
            arr[min_idx] = temp;

        }

    }

} // End of Code

