package com.gdstruc.quiz3;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner myObj = new Scanner(System.in);
        ArrayQueue queue = new ArrayQueue(7);

        int matches = 0;

        while (matches != 10){

            System.out.println("Number of matches made: " + matches );

            int random = (int) (Math.random() * (7 - 1 + 1) + 1);

            for (int i = 0; i < random; i++){
                queue.add(new Player(1, "Test Player", 100));
            }

            String pause = myObj.nextLine();

            System.out.println("Players in queue:");
            queue.printQueue();

            myObj.nextLine();

            if(queue.size() >= 5){
                System.out.println("Match found!");
                matches++;

                for (int i = 0; i < 5; i++) {
                    queue.remove();
                }
            }



        }
    }
}
