package com.gdstruc.quiz2;

public class Main {

    public static void main(String[] args) {

        Player asuna = new Player(1, "Asuna", 100);
        Player lethalBacon = new Player(2, "LethalBacon", 205);
        Player hpDeskjet = new Player(3, "HPDeskJet", 34);
        Player heathCliff = new Player(4, "Heathcliff", 69);
        Player test = new Player(5, "test", 45);

        PlayerLinkedList playerLinkedList = new PlayerLinkedList();

        playerLinkedList.addVar(asuna);
        playerLinkedList.addVar(lethalBacon);
        playerLinkedList.addVar(hpDeskjet);
        playerLinkedList.addVar(heathCliff);

        playerLinkedList.printList();

        //System.out.println(playerLinkedList.containsElement(lethalBacon)); //CONTAINS
        //System.out.println(playerLinkedList.indexOf(lethalBacon)); //INDEX OF

        playerLinkedList.countElements(); //COUNT ELEMENTS
        playerLinkedList.removeFirstElement(); //REMOVE FIRST ELEMENT
        playerLinkedList.printList();
        playerLinkedList.countElements(); //RECOUNT ELEMENTS


    }

}
