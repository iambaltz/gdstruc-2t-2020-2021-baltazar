package com.gdstruc.quiz2;

public class PlayerLinkedList {
    private PlayerNode head;
    private PlayerNode tail;

    public void addVar(Player player) {

        PlayerNode playerNode = new PlayerNode(player);

        if (head == null){

            head = tail = playerNode;
            head.setPreviousPlayer(null);
            tail.setNextPlayer(null);
        }

        else{

            tail.setNextPlayer(playerNode);
            playerNode.setNextPlayer(tail);
            tail = playerNode;
            tail.setNextPlayer(null);

        }

    }

    public void printList() {

        PlayerNode current = head; //Set current as head
        System.out.print("Head -> ");
        while (current != null) {
            System.out.print(current.getPlayer());
            System.out.print(" -> ");
            current = current.getNextPlayer(); //Set the next one after the head as current
        }
        System.out.println("null");
    }

    public void countElements(){

        PlayerNode current = head;
        int count = 0;

        while(current != null){
            count++;
            current = current.getNextPlayer();
        }
        System.out.println(count);
    }

    public void removeFirstElement(){
        PlayerNode current = head;
        head = current.getNextPlayer();
    }

    public boolean containsElement(Player player){
        PlayerNode current = head;
        boolean result = false;
        while(current != null){

            if(player.getId() == current.getPlayer().getId() && player.getLevel() == current.getPlayer().getLevel() && player.getName() == current.getPlayer().getName()){
                return true;
            }

            current = current.getNextPlayer();
        }

            return false;

    }

    public int indexOf(Player player){

        PlayerNode current = head;
        int count = 0;

        while(current != null){

            if(player.getId() == current.getPlayer().getId() && player.getLevel() == current.getPlayer().getLevel() && player.getName() == current.getPlayer().getName()){
                return count;
            }

            count ++;
            current = current.getNextPlayer();
        }
        return -1;
    }

}
