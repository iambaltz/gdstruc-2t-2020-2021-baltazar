package com.gdstruc.quiz6;

public class Tree {
    private Node root;

    public void insert(int value) {

        if (root == null) {

            root = new Node(value);

        } else {

            root.insert(value);

        }
    }

    public Node get(int value) {
        if (root != null)
        {
            return root.get(value);
        }
        return null;
    }

    public void traverseInOrder(){

        if (root != null){

            root.traverseInOrderDescending();

        }

    }

    public Node getRoot() {
        return root;
    }

    public void setRoot(Node root) {
        this.root = root;
    }

    public int getMax(Node node){

        if(node == null){
            return Integer.MIN_VALUE;
        }

        int x = node.getData();
        int y = getMax(node.getLeftChild());
        int z = getMax(node.getRightChild());

        if (y > x){
            x = y;
        }
        if(z > x){
            x = z;
        }
        return x;

    }

    public int getMin(Node node){
        if(node == null){
            return Integer.MAX_VALUE;
        }

        int x = node.getData();
        int y = getMin(node.getLeftChild());
        int z = getMin(node.getRightChild());

        if(y < x){
            x = y;
        }
        if(z < x){
            x = z;
        }
        return x;
    }


}
